extends KinematicBody2D

onready var Player = get_parent().get_node("Player")

const GRAVITY = 1200
const SPEED = 50
const UP = Vector2(0, -1)

var velocity = Vector2()
var jump_speed = -500
var current_phase = "1"
var phase_multiplier = 1.3

var is_dead = false

var direction = 0 
var react_time = 400
var next_direction = 0
var next_direction_time = 0

func taking_damage(damage):
	if $Health.health > 0:
		$Health.take_damage(damage)
	else:
		dead()

func dead():
	is_dead = true
	$CollisionShape2D.disabled = true
	velocity = Vector2(0, 0)
	$AnimatedSprite.play(str("Dead Phase " + current_phase))
	$Timer.start()

func set_dir(target_dir):
	if next_direction != target_dir:
		next_direction = target_dir
		next_direction_time = OS.get_ticks_msec() + react_time

func _process(delta):
	velocity.x = 0
	if is_on_floor() and is_on_wall() and Player.position.y < position.y:
		velocity.y = jump_speed
	if Player.position.x < position.x and next_direction != -1:
		set_dir(-1)
		$AnimatedSprite.flip_h = true
	elif Player.position.x > position.x and next_direction != 1:
		set_dir(1)
		$AnimatedSprite.flip_h = false
	elif Player.position.x == position.x and next_direction != 0:
		set_dir(0)
		
	if OS.get_ticks_msec() > next_direction_time:
		direction = next_direction

func _physics_process(delta):
	if is_dead == false:
		velocity.x = SPEED * direction * phase_multiplier * int(current_phase)
		
		velocity.y += delta * GRAVITY
	
		if velocity.x != 0:
			$AnimatedSprite.play(str("Run Phase " + current_phase))
		elif velocity.y != 0:
			$AnimatedSprite.play(str("Jump Phase " + current_phase))
			
		velocity = move_and_slide(velocity, UP)


func _on_Timer_timeout():
	if current_phase == "3":
		get_tree().change_scene(str("res://Scenes/Win.tscn"))
	else:
		current_phase = str(int(current_phase) + 1)
		is_dead = false
		$Health.change_phase(int(current_phase))
	


func _on_Area2D_body_entered(body):
	if "Player" in body.name:
		get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
