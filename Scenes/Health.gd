extends Node

const START_HEALTH = 50

var health = 50

signal on_health_changed(health)
signal update_max_health(health)

func take_damage(damage):
	health -= damage
	emit_signal("on_health_changed", health)
	
func change_phase(phase):
	health = START_HEALTH * phase
	emit_signal("update_max_health", health)