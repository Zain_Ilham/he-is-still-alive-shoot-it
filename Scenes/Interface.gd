extends Control

func _on_Health_on_health_changed(health):
	$"Bars/Boss Health Bar/TextureProgress".value = health

func _on_Health_update_max_health(health):
	$"Bars/Boss Health Bar/TextureProgress".max_value = health + 2
	$"Bars/Boss Health Bar/TextureProgress".value = health + 2
