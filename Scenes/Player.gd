extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)
const BULLET = preload("res://Scenes/Bullet.tscn")

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
		$Animator.flip_h = false
		if sign($Position2D.position.x) == -1 :
			$Position2D.position.x *= -1
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		$Animator.flip_h = true
		if sign($Position2D.position.x) == 1 :
			$Position2D.position.x *= -1
	if Input.is_action_just_pressed("fire"):
		var bullet = BULLET.instance()
		if sign($Position2D.position.x) == 1:
			bullet.set_direction(1)
		else:
			bullet.set_direction(-1)
		get_parent().add_child(bullet)
		bullet.position = $Position2D.global_position

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		$Animator.play("Jump")
	elif velocity.x != 0:
		$Animator.play("Walk")
	else:
		$Animator.play("Idle")
